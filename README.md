A collection of links and resources, stored here for ease of access

[TOC]

# Computers/Programming

## Software/Apps

### Todoist
- [Todoist for GTD](https://www.dialexa.com/our-insights/2019/12/9/todoist-for-getting-things-done-gtd-10-top-ways)

## Linux

### General Linux
- [GitHub - trimstray/the-practical-linux-hardening-guide: This guide details creating a secure Linux production system. OpenSCAP (C2S/CIS, STIG).](https://github.com/trimstray/the-practical-linux-hardening-guide)
- [How To Create a Multiboot USB Drive for Linux | Tom's Hardware](https://www.tomshardware.com/how-to/multi-boot-linux-from-usb)

### Vim
- [Differences between VIM and NeoVIM](https://neovim.io/doc/user/vim_diff.html)
- [GitHub - mhinz/vim-galore: All things Vim!](https://github.com/mhinz/vim-galore)

### Emacs

### Bash
- [Wiki of Bash Resources](https://wiki.bash-hackers.org/start)
- [Pure sh bible (How to do things in pure shell)](https://github.com/dylanaraps/pure-sh-bible)
- [GitHub - denysdovhan/bash-handbook: For those who wanna learn Bash](https://github.com/denysdovhan/bash-handbook#streams-pipes-and-lists)

## Python

## C Language

## Latex
- [Being Latex in Minutes](https://github.com/luong-komorebi/Begin-Latex-in-minutes)

## Assembly
- [List of Resources for Assembly/Malware Analysis/Reverse Engineering](https://gist.github.com/muff-in/ff678b1fda17e6188aa0462a99626121)

# Languages

## Latin

## General
